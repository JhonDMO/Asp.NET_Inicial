﻿using MinhaGrana.Filters;
using MinhaGrana.Models;
using System.Web.Mvc;

namespace MinhaGrana.Controllers
{
    public class CadastroController : Controller
    {
        // GET: Cadastro
        [AutorizacaoTipo(new[] { TipoUsuario.Administrador})]
        public ActionResult Index()
        {
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MinhaGrana.ViewModels
{
    public class LoginViewModel
    {
        public string UrlRetorno { get; set; }

        [Required(ErrorMessage = "Informe seu logim de acesso")]
        [MaxLength(50, ErrorMessage = "O login deve ter no maximo 50 caracteres")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Informe sua senha de acesso")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Sua senha deve ter no minimo 6 caracteres")]
        public string Senha { get; set; }
    }
}